# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



# creating promotions
promotions = []
20.times do
  promotions << Promotion.create(
      code: Faker::Commerce.promotion_code(digits: 2),
      discount: 13.0,
      active: Faker::Boolean.boolean(true_ratio: 0.2))
end

# creating products
1000.times do
  p = Product.create(
      name: Faker::Commerce.product_name,
      price: Faker::Commerce.price,
      department: Department.find_or_create_by(name: Faker::Commerce.department)
  )
  # adding a random number of promotions to every product
  get_random = []
  rand(0..3).times do
    while get_random.include?(random_promotion = rand(0..promotions.length - 1))
      # nothing here
    end
    p.promotions << promotions[random_promotion]
    get_random << random_promotion
  end
end