require 'test_helper'

class ProductTest < ActiveSupport::TestCase

  test "get_by_search_param with empty queries" do
    product = create(:product, name: "Counter Strike")
    res = Product.get_by_search_param({})
    assert_equal product, res[:products].first
  end

  test "get product with department query" do
    games_dep = create(:department, name: "Games")
    electronic_dep = create(:department, name: "Electronics")
    # electronic department
    create(:product, name: "Iron Machine", department: electronic_dep)
    # game department product
    create(:product, name: "Counter Strike", department: games_dep)
    # no department
    create(:product, name: "Counter Strike", department: nil)


    res = Product.get_by_search_param({department_name: games_dep.name})

    assert_equal 1, res[:count]
    assert_equal games_dep.name, res[:products].first.department.name
  end

  test "get product with promotion filter" do
    p1 = create(:product, name: "Iron Machine")
    p2 = create(:product, name: "CSGO")
    create(:product, name: "Overwatch")
    promo_1 = create(:promotion, code: "AAA")
    promo_2 = create(:promotion, code: "BBB")

    p1.promotions << promo_1
    p2.promotions << promo_1
    p2.promotions << promo_2

    res = Product.get_by_search_param({promotion_code: promo_1.code})

    assert_equal 2, res[:count]
    assert_includes res[:products], p1
    assert_includes res[:products], p2

  end

  test "should test the product name filter" do
    p1 = create(:product, name: "first name")
    p2 = create(:product, name: "sEcOnD name")

    res1 = Product.get_by_search_param({product_name: "name"})
    res2 = Product.get_by_search_param({product_name: "cond"})


    assert_equal 2, res1[:count]
    assert_includes res1[:products], p1
    assert_includes res1[:products], p2

    assert_equal 1, res2[:count]
    assert_includes res2[:products], p2

  end



end
