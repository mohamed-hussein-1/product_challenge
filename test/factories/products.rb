# This will guess the User class
FactoryBot.define do
  factory :product do
    after(:build) do |product|
      product.department = build(:department) if product.department.nil?
    end
  end
end