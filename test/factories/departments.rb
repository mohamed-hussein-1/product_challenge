# This will guess the User class
FactoryBot.define do
  factory :department do
    name {"Flower"}
    trait :for_product do
      association(:products, factory: :product)
    end
  end
end