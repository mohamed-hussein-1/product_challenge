class Product < ApplicationRecord
  has_and_belongs_to_many :promotions, join_table: 'products_promotions'
  belongs_to :department

  def self.get_by_search_param(search_params)
    query = Product
    if search_params[:department_name].present?
      query = query.joins(:department).where(departments: {name: search_params[:department_name]})
    end
    if search_params[:promotion_code].present?
      query = query.joins(:promotions).where(promotions: {code: search_params[:promotion_code]})
    end
    if search_params[:product_name].present?
      query = query.where(Product.arel_table[:name].matches("%#{sanitize_sql_like search_params[:product_name]}%"))
    end
    count_query = query.count
    page_obj = {page: 1, per_page: 10}
        .merge(
            {
                page_number: search_params[:page_number] || 1,
                per_page: search_params[:per_page] || 10
            })
    query = query.paginate(page: page_obj[:page_number], per_page: page_obj[:per_page])
    query = query.order(created_at: :desc)
    query = query.select(Product.arel_table[:id])
    {
        products: Product.where(id: query).includes(:department, :promotions),
        page_number: page_obj[:page_number],
        per_page: page_obj[:page_number],
        count: count_query
    }
  end
end
