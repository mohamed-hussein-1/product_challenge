class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  def index
    products_query = Product.get_by_search_param(index_params)
    @products = products_query[:products]
    total = products_query[:count]
    render json: {
        products: @products.as_json(include: [:department, :promotions]),
        count: total,
        page_number: products_query[:page_number],
        per_page: products_query[:per_page]
    }
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :price, :department_id)
    end

    def index_params
      params.permit(:department_name, :promotion_code, :product_name, :page_number, :per_page)
    end
end
