# README

To run the project ensure you have ruby 2.6.5 ( included in `.ruby-version` file)

after installing ruby run

`rails db:migrate`

to ensure tests run fine

`rake db:test`

seed database to get data you can view through front end

`rake db:seed`

run rails and Voila

`rails s`


**Note**

project is using sqlite instead of postgres because it is easier to be installed.
